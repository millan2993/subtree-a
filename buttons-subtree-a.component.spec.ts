import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsSubtreeAComponent } from './buttons-subtree-a.component';

describe('ButtonsSubtreeAComponent', () => {
  let component: ButtonsSubtreeAComponent;
  let fixture: ComponentFixture<ButtonsSubtreeAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonsSubtreeAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsSubtreeAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
